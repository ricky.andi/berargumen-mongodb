Berargumen

Aplikasi untuk visualisasi argumen

- Setup development
	1) install node js versi terbaru
	2) install bower, nodemon (npm install -g bower nodemon)
	3) install semua dependency untuk frontend `bower install`
	4) install semua dependency untuk backend `npm install`
	5) Sebelumnya, jalankan MongoDB terlebih dahulu
	6) untuk start server, jalankan perintah `npm start`